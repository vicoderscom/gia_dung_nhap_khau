<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style() {
	wp_enqueue_style(
		'template-style',
		asset('app.css'),
		false
	);
}

function theme_enqueue_scripts() {
	wp_enqueue_script(
		'template-scripts',
		asset('app.js'),
		'jquery',
		'1.0',
		true
	);

	$protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

	$params = array(
		'ajax_url' => admin_url('admin-ajax.php', $protocol),
	);
	wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
	/**
	 * setup support for theme
	 *
	 * @return void
	 */
	function themeSetup() {
		// Register menus
		register_nav_menus(array(
			'main-menu' => __('Main Menu', 'giadung'),
		));

		// add_theme_support('menus');
		add_theme_support('post-thumbnails');
		add_image_size('product', 300, 250, true);
		add_image_size('post', 193, 121, true);
	}

	add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
	/**
	 * register sidebar for theme
	 *
	 * @return void
	 */
	function themeSidebars() {
		$sidebars = [
			[
				'name' => __('Sidebar', 'giadung'),
				'id' => 'main-sidebar',
				'description' => __('Main Sidebar', 'giadung'),
				'before_widget' => '<article id="%1$s" class="widget %2$s">',
				'after_widget' => '</article>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
			],
			[
				'name' => __('Footer Gift', 'vicoders'),
				'id' => 'gift-sidebar',
				'description' => __('Footer Gift', 'vicoders'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2 class="widget-title">',
				'after_title' => '</h2>',
			],
			[
				'name' => __('Footer Blog', 'vicoders'),
				'id' => 'blog-sidebar',
				'description' => __('Blog Gift', 'vicoders'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2 class="widget-title">',
				'after_title' => '</h2>',
			],
			[
				'name' => __('Map Contact', 'vicoders'),
				'id' => 'map-contact',
				'description' => __('Map Contact', 'vicoders'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2 class="widget-title">',
				'after_title' => '</h2>',
			],
			[
				'name' => __('Email Subcribes', 'vicoders'),
				'id' => 'email-subcribe',
				'description' => __('Email Subcribes', 'vicoders'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2 class="widget-title">',
				'after_title' => '</h2>',
			],
		];

		foreach ($sidebars as $sidebar) {
			register_sidebar($sidebar);
		}
	}

	add_action('widgets_init', 'themeSidebars');
}

if (!function_exists('registerCustomizeFields')) {
	function registerCustomizeFields() {
		$data = [
			[
				'info' => [
					'name' => 'header_customize',
					'label' => 'Header',
					'description' => '',
					'priority' => 1,
				],
				'fields' => [
					[
						'name' => 'logo',
						'type' => 'upload',
						'default' => '',
						'label' => 'Logo',
					],
					[
						'name' => 'full_logo',
						'type' => 'text',
						'default' => '',
						'label' => 'Full Logo',
					],
					[
						'name' => 'dai_dien',
						'type' => 'text',
						'default' => '',
						'label' => 'Đại diện',
					],
					[
						'name' => 'slogan',
						'type' => 'text',
						'default' => '',
						'label' => 'Slogan',
					],
					[
						'name' => 'create_time',
						'type' => 'text',
						'default' => '',
						'label' => 'Năm thành lập',
					],
					[
						'name' => 'address',
						'type' => 'text',
						'default' => '',
						'label' => 'Địa chỉ',
					],
					[
						'name' => 'phone1',
						'type' => 'text',
						'default' => '',
						'label' => 'Số điện thoại 1',
					],
					[
						'name' => 'phone2',
						'type' => 'text',
						'default' => '',
						'label' => 'Số điện thoại 2',
					],
					[
						'name' => 'phone3',
						'type' => 'text',
						'default' => '',
						'label' => 'Số điện thoại 3',
					],
				],
			],
			[
				'info' => [
					'name' => 'footer_customize',
					'label' => 'Footer',
					'description' => '',
					'priority' => 2,
				],
				'fields' => [
					[
						'name' => 'brand1',
						'type' => 'upload',
						'default' => '',
						'label' => 'Brand1',
					],
					[
						'name' => 'brand2',
						'type' => 'upload',
						'default' => '',
						'label' => 'Brand2',
					],
					[
						'name' => 'email',
						'type' => 'text',
						'default' => '',
						'label' => 'Email',
					],
					[
						'name' => 'footer_top',
						'type' => 'text',
						'default' => '',
						'label' => 'Footer top',
					],
					[
						'name' => 'copyright',
						'type' => 'text',
						'default' => '',
						'label' => 'Copyright',
					],
				],
			],
		];

		$customizer = new MSC\Customizer\Customizer($data);
		$customizer->create();
	}

	add_action('init', 'registerCustomizeFields');
}
