<?php
	/**
	* 
	*/
class WoocommerceHelpers{	
	public function __construct()
	{
		add_filter( 'woocommerce_breadcrumb_defaults', [$this, 'change_breadcrumb_home_text'] );
		add_filter('woocommerce_product_single_add_to_cart_text',[$this,'change_text_addtocart_single']);
		remove_action("woocommerce_single_product_summary","woocommerce_template_single_price",10);
		add_action("woocommerce_single_product_summary","woocommerce_template_single_price",20);
		remove_action("woocommerce_after_single_product_summary","woocommerce_output_related_products",20);
		remove_action("woocommerce_before_main_content","woocommerce_breadcrumb",20);
		remove_action("woocommerce_single_product_summary", "woocommerce_template_single_meta",40);
		add_action("woocommerce_single_product_summary", "woocommerce_template_single_meta",10);
		add_filter( 'woocommerce_checkout_fields' , [$this,'custom_override_checkout_fields'] );
		add_filter("woocommerce_checkout_fields", [$this,"order_fields"]);
		add_filter( 'wc_empty_cart_message', [$this,'custom_wc_empty_cart_message'] );
		add_filter( 'woocommerce_breadcrumb_defaults', [$this,'my_change_breadcrumb_delimiter'] );
		add_filter( 'woocommerce_add_error', [$this,'customize_wc_errors']);
		add_filter('pre_get_posts',[$this,'SearchFilter']);
		add_filter( 'woocommerce_variable_sale_price_html', [$this,'wc_wc20_variation_price_format'], 10, 2 );
		add_filter( 'woocommerce_variable_price_html', [$this,'wc_wc20_variation_price_format'], 10, 2 );
	}
	public function change_breadcrumb_home_text( $defaults ) {
	    // Change the breadcrumb home text from 'Home' to 'Apartment'
	    $defaults['home'] = 'Trang chủ';
	    return $defaults;
	}
	public function change_text_addtocart_single($text){
	    return 'Mua ngay';
	}
	function my_change_breadcrumb_delimiter( $defaults ) {
		// Change the breadcrumb delimiter from '/' to '>'
		$defaults['delimiter'] = ' > ';
		return $defaults;
	}
	public function custom_override_checkout_fields( $fields ) {
	     $fields['billing']['billing_last_name'] = array(
	        'label'     => __('Họ', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => false,
	    'class'     => array('form-row-last'),
	    'priority'=> 1,
	    'clear'     => true
	     );
	     $fields['billing']['billing_first_name'] = array(
	        'label'     => __('Tên', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => true,
	    'class'     => array('form-row-last'),
	    'priority'=> 1,
	    'clear'     => true
	     );
	     $fields['billing']['billing_country'] = array(
	        'label'     => __('Đất nước', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => false,
	    'class'     => array('form-row-wide'),
	    'clear'     => true
	     );
		$fields['billing']['billing_address_1'] = array(
			'type'=>'text',
		 'label' => __('Địa chỉ giao hàng(số nhà, ngõ, đường, quận/huyện, thành phố)', 'woocommerce'),
		 'placeholder' => _x('', 'placeholder', 'woocommerce'),
		 'required' => true,
		 'class' => array('form-row-wide'),
		 'clear' => true
		 );

	     $fields['billing']['billing_company'] = array(
	     'type' => 'textarea',
	        'label'     => __('Ghi chú', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => false,
	    'class'     => array('form-row-wide note-bill'),
	    'clear'     => true
	     );

	     $fields['billing']['billing_city'] = array(
	     	'type'=>'select',
	        'label'     => __('Hình thức vận chuyển', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => false,
	    'class'     => array('form-row-wide'),
	    'options'=>array(
	    	'moto'=>'Xe máy',
	    	'car'=>'Ô tô',
	    	'post'=>'Bưu điện'
	    ),
	    'clear'     => true
	     );
	     $fields['billing']['billing_phone'] = array(
	        'label'     => __('SĐT', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => true,
	    'class'     => array('form-row-last'),
	    'priority'=> 3,
	    'clear'     => true
	     );
	     $fields['billing']['billing_email'] = array(
	    'label'     => __('Email', 'woocommerce'),
	    'placeholder'   => _x('', 'placeholder', 'woocommerce'),
	    'required'  => true,
	    'class'     => array('form-row-last'),
	    'priority'=> 4,
	    'clear'     => true
	     );
	     unset($fields['billing']['billing_postcode']);
	     unset($fields['billing']['billing_address_2']);
	     unset($fields['billing']['billing_state']);
	     // unset($fields['billing']['billing_country']);
	     return $fields;
	}

	public function order_fields($fields) {

	    $order = array(
	        "billing_first_name", 
	        "billing_last_name", 
	        "billing_phone",
	        "billing_email",
	        "billing_address_1", 
	        // "billing_state",
	        "billing_country",
	        // "billing_address_2", 
	        "billing_city",
	        "billing_company"
	         
	        

	    );
	    foreach($order as $field)
	    {
	        $ordered_fields[$field] = $fields["billing"][$field];
	    }

	    $fields["billing"] = $ordered_fields;
	    return $fields;

	}

	public function custom_wc_empty_cart_message() {
	  return 'Giỏ hàng của bạn trống';
	}
	public function customize_wc_errors( $error ) {
	    if ( strpos( $error, 'Thanh toán ' ) !== false ) {
	        $error = str_replace("Thanh toán ", "", $error);
	    }
	    return $error;
	}
	public function SearchFilter($query) {
	  if ($query->is_search) {
	    // yêu cầu tìm kiếm từ khóa trong sản phẩm
	    $query->set('post_type', 'product');
	  }
	  return $query;
	}
	public function wc_wc20_variation_price_format( $price, $product ) {
		if(is_front_page() || is_category() || is_archive()){
			 //Main Price
			 $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
			 $price = $prices[0] !== $prices[1] ? sprintf( __( 'Giá từ: %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
			 
			 // Sale Price
			 $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
			 sort( $prices );
			 $saleprice = $prices[0] !== $prices[1] ? sprintf( __( 'Giá từ: %1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
			 
			 if ( $price !== $saleprice ) {
			 $price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
			 }
			 
			}
			return $price;
		}
	}
	new WoocommerceHelpers();
?>