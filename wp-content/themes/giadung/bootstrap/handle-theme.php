<?php
/**
 *
 */
class HandleTheme {

	public function __construct() {
		add_action('admin_post_sign_up', [$this, 'prefix_admin_sign_up']);
		add_action('admin_post_nopriv_sign_up', [$this, 'prefix_admin_sign_up']);
		add_filter('login_form_defaults', [$this, 'wpse60605_change_username_label']);
		add_action('login_form_middle', [$this, 'add_lost_password_link']);

		add_action('wp_ajax_check_user_email_callback', [$this, 'check_user_email_callback']);
		add_action('wp_ajax_nopriv_check_user_email_callback', [$this, 'check_user_email_callback']);

		add_action('wp_ajax_check_user_exists', [$this, 'check_user_exists']);
		add_action('wp_ajax_nopriv_check_user_exists', [$this, 'check_user_exists']);
	}
	public function prefix_admin_sign_up() {
		$err = '';
		$success = '';global $wpdb, $user_ID, $users;
		$home_url = get_option('home');
		if (isset($_POST['task']) && $_POST['task'] == 'register') {
			$pass = esc_sql(trim($_POST['password']));
			//hàm escape loại bỏ các kí tự đặc biệt
			// Hàm trim loại bỏ kí tự khoảng trắng
			$repass = esc_sql(trim($_POST['repassword']));
			$email = esc_sql(trim($_POST['email']));
			$username = esc_sql(trim($_POST['username']));

			$user_id = wp_insert_user(array('user_pass' => apply_filters('pre_user_user_pass', $pass), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => 'subscriber'));
			wp_redirect(home_url());
			//Mặc định tất cả các nguwoif đăng kí đều thành theo dõi
			if (is_wp_error($user_id)) {
				echo 'Error on user creation.';

			} else {
				do_action('user_register', $user_id);
				echo 'Bạn đã đăng ký thành công!';
			}
		}
	}
	public function wpse60605_change_username_label($defaults) {
		$defaults['label_username'] = __('Tên đăng nhập');
		return $defaults;
	}
	public function add_lost_password_link() {
		return '<p><a href="/wp-login.php?action=lostpassword">Quên mật khẩu?</a></p>';
	}

	public function check_user_email_callback() {
		global $wpdb; // this is how you get access to the database

		if (email_exists($_POST['email'])) {
			echo json_encode('Email đã tồn tại');
		} else {
			echo json_encode('true');
		}
		die();
	}

	public function check_user_exists() {
		global $wpdb;

		$username = $_POST['username'];

		if (username_exists($username)) {
			echo json_encode('user đã tồn tại');
		} else {
			echo json_encode('true');
		}
		die();

	}
}
new HandleTheme();
?>