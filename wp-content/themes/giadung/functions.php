<?php

$app = require_once __DIR__ . '/bootstrap/app.php';

add_action('pre_get_posts', function ($query) {
    if ($query->is_search) {
        $query->set('posts_per_page', 9);
    }
});

