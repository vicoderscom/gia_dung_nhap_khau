import 'jquery';
import 'bootstrap';
import slick from "slick-carousel/slick/slick.js";

import meanmenu from 'jquery.meanmenu/jquery.meanmenu.min.js';
import ezPlus from "ez-plus/src/jquery.ez-plus.js";
import 'jquery-validation/dist/jquery.validate.js';


$(document).ready(function () {
	$('.tax-list li').find('.tax-sub').parent().addClass('tax-sub-hover');

    $('.banner-content').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        pauseOnHover: true,
        arrows: false,
    });
    $('.main-menu').meanmenu({
        meanScreenWidth: "1024",
        meanMenuContainer: ".mobile-menu",
    });    
                   
    $("#signup-form").validate({
      rules: {
          username: {
             required : true,
             remote: {
              url: ajax_obj.ajax_url,
              type: "post",
              data: {
                 'username': function() {
                      return $( "#username" ).val();
                  },
                 'action': 'check_user_exists'

              }
            }
          },
          email : {
            required: true,
             email: true,
             remote: {
              url: ajax_obj.ajax_url,
              type: "post",
              data: {
                 'email': function() {
                      return $( "#email" ).val();
                  },
                 'action': 'check_user_email_callback'

              }
            }
          },
          password : {
            required: true,
            minlength :6
          },
          repassword : {
              required: true,
              minlength: 6,
              equalTo: "#password"
          }
      },
      messages : {
          username: {
            required: "Vui lòng không để trống ô này",
            remote : "Tài khoản này đã tồn tại"
          },
          email: {
            required: "Vui lòng không để trống ô này"
          },
          password: {
            required: "Vui lòng không để trống ô này", 
            minlength: "Mật khẩu tối thiểu phải là 6 kí tự"
          },
          repassword: {
              required: "Vui lòng không để trống ô này",
              minlength: "Mật khẩu tối thiểu phải là 6 kí tự",
              equalTo: "Mật khẩu không giống với mật khẩu ở trên"
          }
      }
    }); 
    jQuery('#user_login').attr('placeholder', 'Tên đăng nhập....'); 
    jQuery('#user_pass').attr('placeholder', 'Mật khẩu ....');
    // jQuery('#user_login').attr('lable', 'Tên đăng nhập'); 
});