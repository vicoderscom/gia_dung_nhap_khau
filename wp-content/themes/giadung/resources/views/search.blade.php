@extends('layouts.full-width')

@section('content')

    <div class="page-search">
        <div class="container">
            <div class="row">
                <?php get_sidebar();?>

                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 home-tax-content search-content"> 
                    <div class="home-tax-title">
                        <h2>Kết quả tìm kiếm</h2>
                    </div>
                    <h1 class="entry-title">
                        @php
                           global $wp_query;
                        @endphp
                        <?php echo "Hiển thị ".(int)$wp_query->found_posts." kết quả cho từ khóa '".$_GET['s']."'"; ?>
                    </h1>
                    <div class="row">
                        <div class="msc-listing">
                            @if(have_posts()) 
                                @while(have_posts())
                                    
                                    {!! the_post() !!}
                                    
                                    {{ view('partials.content-search') }}
                                @endwhile
                            @else
                                {{ 'Không có kết quả nào' }}
                            @endif
                            {{ view('partials.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>  
    
@endsection

                