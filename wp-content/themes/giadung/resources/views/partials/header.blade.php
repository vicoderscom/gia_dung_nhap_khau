<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="address">
                {{ get_option('header_customize_address') }}
            </div>
            <div class="phone">
              <?php
$phone1 = get_option('header_customize_phone1');
$phone2 = get_option('header_customize_phone2');
$phone3 = get_option('header_customize_phone3');

$a = preg_replace('/\s+/', '', $phone1);
$b = preg_replace('/\s+/', '', $phone2);
$c = preg_replace('/\s+/', '', $phone3);
?>
              <a href="tel:{!! $a !!}">
                {{ "Call: ".get_option('header_customize_phone1')." - " }}
              </a>
              <a href="tel:{!! $b !!}">
                {{ get_option('header_customize_phone2')." - " }}
              </a>
              <a href="tel:{!! $c !!}">
                {{ get_option('header_customize_phone3') }}
              </a>
            </div>
        </div>
    </div>
  <div class="header-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 logo">
                <a href="{{ get_option('home') }}">
                    <img src="{{ get_option('header_customize_logo') }}" alt="logo" class="img_logo">
                </a>
                <div class="full_logo">
                    <p class="text_logo">{{ get_option('header_customize_full_logo') }}</p>
                </div>
                <div class="dai_dien">
                  <p class="dai_dien_doc_quyen">
                    {{ get_option('header_customize_dai_dien') }}
                  </p>
                </div>

            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 brand">
                <p class="thuong_hieu">{{ get_option('header_customize_slogan') }}</p>
                <p>{{ get_option('header_customize_create_time') }}</p>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 search">
                <div class="header-cart">
                  <div class="login">
                    @php
                    global $user_id, $current_user, $current_url;
                       if(is_user_logged_in()) {
                        $user_id = get_current_user_id();
                        $current_user = wp_get_current_user();
                        $profile_url = get_author_posts_url($user_id);
                        $edit_profile_url  = get_edit_profile_url($user_id);
                    @endphp
                    <div class="regted">
                    <a href="{{ $profile_url }}">{{ $current_user->display_name }}</a>
                    <a href="{{ wp_logout_url(home_url()) }}" > (Thoát)</a>
                    </div>
                   @php }else{ @endphp
                    <a href="" data-toggle="modal" data-target="#login">Đăng nhập/ </a>
                    <a href="" data-toggle="modal" data-target="#signup">Đăng kí</a>
                    @php } @endphp
                  </div>
                  <div class="cart">
                    <div class="cart-icon">
                      <a href="{{ esc_url( wc_get_cart_url() ) }}">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="cart-total">
                      {{ wp_kses_data ( WC()->cart->get_cart_contents_count() ) }}
                    </div>
                  </div>
                </div>
                <div class="header-search">
                  <div class="searchbox">
                        <form action="{{ esc_url( home_url( '/' ) ) }}">
                            <input type="text" placeholder="Tìm kiếm..." name="s" required requiredmsg=" <?php echo 'Bạn vui lòng không để trống ˚¬˚ '; ?>" value="{{ get_search_query() }}">
                            <button type="submit" class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                 </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="header-menu">
    <div class="container">
      <nav class="menu">
            <div class="main-menu">
                @if (has_nav_menu('main-menu'))
                    {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
                @endif
            </div>
            <div class="mobile-menu"></div>
      </nav>
    </div>
  </div>
</header>

<!-- Modal -->

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">X<span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Đăng nhập</h4>
      </div>
      <div class="modal-body form">
        @php
            wp_login_form()
        @endphp
      </div>
      <div class="modal-footer">
        <a href="" data-toggle="modal" data-target="#signup">Đăng kí</a><span>để mua hàng và nhận nhiều ưu đãi hơn</span>
      </div>
    </div>
  </div>
</div>
<!-- Modal Sign Up -->
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Đăng kí</h4>
      </div>
      <div class="modal-body form">
        <form action="{{ site_url('wp-admin/admin-post.php') }}" name="signup-form" id="signup-form" method="post">
            <input type="hidden" name="action" value="sign_up">
            <p><label for="username" class="label">Tên đăng nhập</label><br>
                <input type="text" placeholder="Tên đăng nhập..." name="username" id="username" class="input_login">
            </p>
           <p> <label for="email" class="label">Email</label><br>
                <input type="text" placeholder="Email..." name="email" id="email" class="input_login"
                >
            </p>
           <p> <label for="password" class="label">Mật khẩu</label><br>
                <input type="password" placeholder="Mật khẩu..." name="password" id="password" class="input_login">
            </p>
           <p> <label for="repassword" class="label">Nhập lại mật khẩu</label><br>
                <input type="password" placeholder="Nhập lại mật khẩu" name="repassword" class="input_login">
            </p>
            <p class="dang_nhap"><button type="submit" name="register" class="register">Đăng kí</button>
              <input type="hidden" name="task" value="register" />
            </p>
        </form>
      </div>
    </div>
  </div>
</div>
