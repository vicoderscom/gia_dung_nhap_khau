<article class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 item">
	<div class="item-content">
		<figure>
			<a href="<?php the_permalink();?>">
				<img src="<?php echo asset2('images/3x3.png'); ?>" alt="<?php echo the_title(); ?>" style="background-image: url(<?php echo getPostImage(get_the_ID(), 'product'); ?>);" />
			</a>
		</figure>

		<div class="p-info">
			<div class="p-msp">
				Mã SP: <span> <?php echo get_field('ma_sp'); ?> </span>
			</div>
			<div class="p-title">
				<a href="<?php the_permalink();?>">
			    	<h3><?php echo the_title(); ?></h3>
			    </a>
			</div>
			<div class="p-price">
				@php
					$money =  wc_get_product(get_the_ID());
					$oldprice = (float)$money->get_regular_price();
				@endphp

				{{ number_format($oldprice) }}
				{{ ' '.get_woocommerce_currency_symbol() }}
				
			</div>
			<div class="p-button">
				<a href="<?php the_permalink();?>">Chi tiết</a>
				{{ view('partials.dat-hang') }}
			</div>

		</div>
	</div>
</article>	

