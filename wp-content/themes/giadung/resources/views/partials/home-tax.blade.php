<div class="home-tax">

    @php
        $list_tax = get_field('list_tax');
    @endphp

    @foreach ($list_tax as $list_tax_home)

        @php
        	$list_tax = get_term($list_tax_home['taxonomy']);

        	$list_tax_id = $list_tax->term_id;
        	$list_tax_name = $list_tax->name;
    	@endphp

    	<div class="home-tax-item">
    		<div class="home-tax-title">
				<h2>{{ $list_tax_name }}</h2>
    			<a href="{{ get_term_link(get_term($list_tax_id)) }}">Xem thêm</a>
    		</div>
    		<div class="home-tax-content">
    			<div class="row">
	                @php
                        $shortcode = '[listing post_type="product" taxonomy="product_cat('.$list_tax_id.')"  per_page="3" layout="partials.content-tax-home"]';
                        echo do_shortcode($shortcode);
	                @endphp
                </div>
    		</div>
    	</div>

    @endforeach

</div>