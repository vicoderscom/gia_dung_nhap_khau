@php
	$star = get_field('danh_gia');
@endphp
<article class="item">
	<div class="row">
        <figure class="col-md-2 col-sm-2 col-xs-2 col-4">
			<a href="{{ $url }}" title="{{ $title }}">
				<img src="{{ asset2('images/3x3.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'post') }});" />
			</a>
        </figure>
        <div class="col-md-10 col-sm-10 col-xs-10 col-8 info">
            <div class="title">
                    <h3>{{ $title }}</h3>
            </div>
            <div class="star">
                <?php if($star==1){ ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                <?php }elseif ($star==2) { ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
               <?php }elseif ($star==3) { ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
               <?php }elseif ($star==4) { ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
               <?php }elseif ($star==5) { ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
               <?php } ?>

            </div>      
            <div class="desc">
            	{{ the_content() }}
            </div>
        </div>
     </div>
</article>


