<footer class="footer">
	<div class="footer-top">
		<p>{{ get_option('footer_customize_footer_top') }}</p>
	</div>
	<div class="footer-midle">
		<div class="container">
			<div class="row">
				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-6 col-xs-12 col-12 doitac">
					<img src="{{ get_option('footer_customize_brand1') }}" alt="Brand1" class="img_logo">
					<img src="{{ get_option('footer_customize_brand2') }}" alt="Brand2" class="img_logo">
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12 col-12 info">
		              <?php 
			              $phone1 =  get_option('header_customize_phone1'); 
			              $phone2 = get_option('header_customize_phone2');
			              $phone3 = get_option('header_customize_phone3');

			              $a = preg_replace('/\s+/', '', $phone1);
			              $b = preg_replace('/\s+/', '', $phone2);
			              $c = preg_replace('/\s+/', '', $phone3);
		              ?>
						<h2 class="widget-title">
							{{ get_option('header_customize_full_logo') }}
						</h2>
					<span class="address">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						{{ get_option('header_customize_address') }}
					</span>
					<span class="phone">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<a href="tel:{!! $a !!}">
							 {{ get_option('header_customize_phone1')." - " }}
						</a>
						<a href="tel:{!! $b !!}">
							{{ get_option('header_customize_phone2') }}
						</a>
					</span>
					<span class="email">
						<i class="fa fa-envelope" aria-hidden="true"></i>
		    			<a href="mailto:{{ get_option('footer_customize_email') }}">
		    				{{ get_option('footer_customize_email') }}
		    			</a>
					</span>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-sx-12 col-12 gift">
					<?php dynamic_sidebar('gift-sidebar'); ?>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-sx-12 col-12 blog">
					 <?php dynamic_sidebar('blog-sidebar'); ?>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-sx-12 col-12 subcribe">
					<div class="email">
						<p>Đăng kí email để nhận thông báo khuyến mại</p>
						<?php dynamic_sidebar('email-subcribe'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<p>{{ get_option('footer_customize_copyright') }}</p>
	</div>
</footer>

