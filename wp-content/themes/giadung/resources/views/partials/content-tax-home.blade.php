<?php global $product;?>
<article class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12 item">
	<div class="item-content">
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset2('images/3x3.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'product') }});" />
			</a>
		</figure>

		<div class="p-info">
			<div class="p-msp">
				Mã SP: <span> {{ get_field('ma_sp') }} </span>
			</div>
			<div class="p-title">
				<a href="{{ $url }}">
			    	<h3>{{ $title }}</h3>
			    </a>
			</div>
			<div class="p-price">
				<p class="price"><?php echo $product->get_price_html(); ?></p>
			</div>
			<div class="p-button">
				<a href="{{ $url }}">Chi tiết</a>
				{{ view('partials.dat-hang') }}
			</div>

		</div>
	</div>
</article>