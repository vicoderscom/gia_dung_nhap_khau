@extends('layouts.full-width')

@section('content')
<div class="single">
	<div class="container">
	    @while(have_posts())
		
			{!! the_post() !!}

	        @include('partials.content-single-' . get_post_type())

	    @endwhile
	</div>
</div>

@endsection
