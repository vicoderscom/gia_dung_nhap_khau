<aside class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 box-sidebar">

	<article class="widget widget-tax">
		<h3 class="widget-title">Sản phẩm</h3>
		<?php
			$taxonomy_name = 'product_cat';
			$terms = get_terms('product_cat', array(
			    'parent'=> 0,
			    'hide_empty' => false
			) );

			echo '<ul class="tax-list">';
				foreach($terms as $term){
					$team_lg = $term->term_id;

					echo '<li><a href="'.get_term_link(get_term($term->term_id)).'">'.$term->name.'</a>';
					
					$term_childs = get_term_children( $term->term_id, $taxonomy_name );
					$count = count($term_childs);
					if($count > 0) {

						echo '<ul class="tax-sub">';
							foreach ( $term_childs as $child ) {
							    $term = get_term_by( 'id', $child, $taxonomy_name );
							    if($term->parent == $team_lg){

									echo '<li><a href="'.get_term_link(get_term($term->term_id)).'">'.$term->name.'</a></li>';

								}
							}
						echo '</ul>';
					}
					echo '</li>';
				}
			echo '</ul>';
		?>
	</article>

	<?php dynamic_sidebar('main-sidebar'); ?>

</aside>