@extends('layouts.full-width')


@section('content')

    @while (have_posts())

        {!! the_post() !!}
        <div class="intro-page">
        	<div class="container">
        		{{ view('partials.page-header') }}
        		{{ the_content() }}
        	</div>
        </div>
    @endwhile
    
@endsection