@extends('layouts.app')

@section('content')

  @if (!have_posts())
  <section class="page-content">
  	<div class="error-page">
	<div class="home-tax-title">
	  <h2>404 Error</h2>
	</div>
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					
				</div>
				<div class="col-md-6">
					<div class="text">
						<p class="p1">
							Error 404
						</p>
						<p class="p2 p1">
							Không tìm thấy trang
						</p>
						<p class="p3">
							Rất tiếc, trang bạn tìm kiếm hiện đã bị xóa hoặc không tìm thấy
						</p>
					</div>
					<div class="img">
						<img src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/icon-404.png' ?>" alt="">
					</div>
				</div>
				<div class="col-md-3">
					
				</div>
			</div>
		</div>
	</div>
</section>
  @endif

  {!! get_the_posts_navigation() !!}
  
@endsection
