<!DOCTYPE html>

<html {!! language_attributes() !!}>

  @include('partials.head')

    <body {!! body_class() !!}>

    {!! do_action('get_header') !!}

    @include('partials.header')

    <div class="wrap fluid-container" role="document">
        <?php if(!is_front_page()){ ?>
        <div class="breadcrumb">
            <div class="container">
                {{ woocommerce_breadcrumb() }}
            </div>      
        </div>
        <?php } ?>
        <div class="content">
            <main class="main">
                @yield('content')
            </main>
        </div>
    </div>

    {!! do_action('get_footer') !!}

    @include('partials.footer')

    {!! wp_footer() !!}

    </body>
    
</html>
