@extends('layouts.full-width')

@section('content')

    @while (have_posts())

        {!! the_post() !!}

			<div class="front-page-content">
				<div class="container">
					{{ wc_print_notices() }}
					<div class="row">
						<?php get_sidebar();?>

						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 home-content">

							<div class="banner">
								<div class="banner-content">
							        @php
							            $anh_banner = get_field('anh_banner');
							            foreach($anh_banner as $img_banner) {
							        @endphp
			                        	<img src="{{ asset2('images/2x1.png') }}" style="background-image: url({{ $img_banner['url'] }});" />
							        @php
							            }
							        @endphp
								</div>
							</div>

							{{ view('partials.home-tax') }}

						</div>

					</div>
				</div>
			</div>

    @endwhile
    
@endsection