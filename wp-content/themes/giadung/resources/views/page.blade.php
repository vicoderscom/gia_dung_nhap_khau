@extends('layouts.full-width')

@section('content')

    @while(have_posts())

		{!! the_post() !!}
		<div class="container">

	        @include('partials.page-header')

	        @include('partials.content-page')
        </div>

    @endwhile

@endsection
