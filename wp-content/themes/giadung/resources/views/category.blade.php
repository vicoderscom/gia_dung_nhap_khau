@extends('layouts.full-width')

@php
    $current_category = get_category_by_slug( get_query_var( 'category_name' ) );
    $cat_id = $current_category->term_id;
    $cat_name = get_cat_name($cat_id);
@endphp
@section('content')
        <div class="category-custom">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-2">
        				
        			</div>
        			<div class="col-md-8">
		        		<h2 class="header-title">Khách hàng nói gì về chúng tôi?</h2>
		                @php
		                    $shortcode = "[listing cat=$cat_id layout='partials.content-category' paged='yes' per_page='4']";
		                    echo do_shortcode($shortcode);
		                @endphp
        			</div>
        			<div class="col-md-2">
        				
        			</div>
        		</div>
        	</div>
        </div>
    
@endsection