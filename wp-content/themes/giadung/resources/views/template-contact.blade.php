@extends('layouts.full-width')


@section('content')

    @while (have_posts())

        {!! the_post() !!}
<div class="contact-page">
	<div class="container">
        {{ view('partials.page-header') }}
        <div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 contact">
				<div class="content-contact">
                    <span class="address">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        {{ get_option('header_customize_address') }}
                    </span>
                    <span class="phone">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <a href="tel:{{ get_option('header_customize_phone1') }}">
                             {{ get_option('header_customize_phone1')." - ".get_option('header_customize_phone2') }}
                        </a>
                    </span>
                    <span class="email">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <a href="{{ get_option('footer_customize_email') }}">
                            {{ get_option('footer_customize_email') }}
                        </a>
                    </span>
                </div>
				<?php the_content(); ?>
			</div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 map">
                <?php dynamic_sidebar('map-contact'); ?>
            </div>

        </div>
	</div>
</div>
    @endwhile
    
@endsection