<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
$p_info = get_field('thong_tin_chi_tiet');
$img = get_field('anh_mo_ta');
$col_p_info = "col-md-6";
?>
<div class="product-info">
	<?php if(!empty($p_info)){ ?>
	<h2 class="single-title">Thông tin chi tiết</h2>
	<div class="product-info-content">
		<div class="row">
			<?php if (!empty($img)) { ?>
				<div class="<?php echo $col_p_info; ?> info">
					<?php echo $p_info; ?>
				</div>
				<div class="col-md-6">
					<img src="<?php echo $img['url']; ?>" alt="">
				</div>
			<?php }else{ ?>
				<div class="<?php $col_p_info = "col-md-12"; echo $col_p_info; ?>">
					<?php echo $p_info; ?>
				</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
</div>
	<div class="thong-so">
		<?php  
			$thong_so_ki_thuat = get_field('thong_so_ki_thuat');
			if(!empty($thong_so_ki_thuat)){
		?>
		<h2 class="single-title">Thông số kĩ thuật</h2>
		
        <ul class="thong-so-list">
            <?php
                foreach ($thong_so_ki_thuat as $tskt) {
                	if($tskt['ten_truong'] != '' && $tskt['thong_tin_truong'] != '') {
                    	echo '
                    	<li><span class="name-field-duan">'.$tskt['ten_truong'].': </span>
                            <span class="info-field-duan">'.$tskt['thong_tin_truong'].'</span>
                        </li>';
                    }
                }
                
            ?>
        </ul>
        <?php } ?>
		
	</div>
