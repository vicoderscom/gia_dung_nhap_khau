	<div class="related-product">
		<h2>Sản phẩm liên quan</h2>
		<div class="home-tax-content related-content">
			<div class="row">
				<div class="msc-listing">

				<?php
global $post;
$terms = get_the_terms($post->ID, 'product_cat', 'string');
$term_ids = wp_list_pluck($terms, 'term_id');
$query = array(
	'post_type' => 'product',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_cat',
			'field' => 'id',
			'terms' => $term_ids,
			'operator' => 'IN',
		)),
	'posts_per_page' => 4,
	'orderby' => 'date',
	'post__not_in' => array($post->ID),
);
if ($query['post_type'] == 'post') {
	add_action('pre_get_posts', function ($query1) {
		$query1->set('posts_per_page', 4);
	});
}
$query2 = new WP_Query($query);
?>

				<?php if ($query2->have_posts()): while ($query2->have_posts()): $query2->the_post();?>

														<article class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 item">
															<div class="item-content">
																<figure>
																	<a href="<?php the_permalink();?>">
																		<img src="<?php echo asset2('images/3x3.png'); ?>" alt="<?php echo the_title(); ?>" style="background-image: url(<?php echo getPostImage(get_the_ID(), 'product'); ?>);" />
																	</a>
																</figure>

																<div class="p-info">
																	<div class="p-msp">
																		Mã SP: <span> <?php echo get_field('ma_sp'); ?> </span>
																	</div>
																	<div class="p-title">
																		<a href="<?php the_permalink();?>">
																	    	<h3><?php echo the_title(); ?></h3>
																	    </a>
																	</div>
																	<div class="p-price">
																		<?php
		global $product;
		if ($product->is_type('simple')) {
			$money = wc_get_product(get_the_ID());
			$oldprice = (float) $money->get_regular_price();
			echo number_format($oldprice) . ' ' . get_woocommerce_currency_symbol();
		} elseif ($product->is_type('variable')) {
		$min_price = $product->get_variation_price('min', true);
		echo "Giá từ: " . number_format($min_price) . "VNĐ";
	}
	?>
												</div>
												<div class="p-button">
													<a href="<?php the_permalink();?>">Chi tiết</a>
													<a href="<?php echo get_page_link(25); ?>">Đặt hàng</a>
												</div>

											</div>
										</div>
									</article>
									<?php endwhile;
wp_reset_postdata();endif;?>

				</div>
			</div>
		</div>
	</div>