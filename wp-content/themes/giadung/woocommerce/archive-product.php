<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<div class="archive-product">
        <div class="breadcrumb">
            <div class="container">
                <?php  echo woocommerce_breadcrumb() ?>
            </div>      
        </div>
		<div class="container">
			<div class="row">
				<?php get_sidebar();?>

				<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 home-tax-content archive-product-content">
					<?php
						$check_archive = Get_queried_object();
						// var_dump($check_archive);
						if (!empty($check_archive)) {
							$check_archive_id = $check_archive->term_id;
							$check_archive_name = $check_archive->name;
					?>

				    		<div class="home-tax-title">
								<h2>
									<?php echo $check_archive_name; ?>
								</h2>
				    		</div>

				    		<div class="row">

					<?php
							    $shortcode = '[listing post_type="product" taxonomy="product_cat('.$check_archive_id.')" layout="partials.content-tax-home" paged="yes" per_page="9"]';
							    echo do_shortcode($shortcode);
						}
					?>
							</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer( 'shop' ); ?>
